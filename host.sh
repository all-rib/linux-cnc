#!/bin/bash

source funcions.lib

executioner()
{
	case $1 in
		0 )	# Inform my IP
			ssh_exec 'echo -e \"$HOSTNAME\t$CLIENT\" >> online_addr'
			;;

		1 )	# Get tasklist
			ssh_exec 'server.sh "$CLIENT" 1'	# Prepare tasklist
			ssh_get "$TASKLIST"

			if [ -a "$TASKLIST" ]; then
				for COMMAND in $(cat "$TASKLIST"); do
					bash tasklist.sh "$COMMAND"
				done
				rm "$TASKLIST"
			fi
			echo "Nothing else to be done"
			;;

		2)	# Journal delivery
			# do something
			;;

		* )	# Any error
			echo "What? I have not understood you"
			;;
	esac

}

local_action()
{
	case $1 in
		1 )	# Generate new key
			gen_key
			;;
		* )
			;;
	esac

}

connect()
{
	while [ $# -ne 1 ]; do
		COMMANDS[index]=$1
		shift 1
		index=$(( $index + 1 ))
	done
	SERVER=$1

	TRY=0
	while [ $TRY -lt $MAX_TRY ]; do
		if [ "$( ping_addr $SERVER )" == "0" ]; then
			for ACTION in ${COMMANDS[@]}; do
				remote_action $ACTION
			done
			exit
		else
			TRY=$(( $TRY + 1 ))
			sleep $TIME
		fi
	done
	exit
}


main()
{

	# Defualt values

	COMMAND=1
	PINGS=5		# Number of pings
	MAX_TRY=10	# Maximum connection tries
	PORT="22"
	TIME=600

	while [ $# -ne 0 ]; do
		case $1 in
			-c|--client)	# Specify host for personilized configuration
				CLIENT="$2"
				;;

			-p|--port)	#Specify port
				PORT=$2
				;;

			-i|--key)	# Specify private key file
				KEY="$2"
				;;

			-n|--tries)	# Specify the number of connections atempts
				MAX_TRY=$2
				;;

			-P|--pings)	# Specify the number of pings to test if server is alive
				PINGS=$2
				;;

			-t|--time)	# Specify the timeout delay before try again
				TIME=$2
				;;

			-*)
				echo " \"$1\" isn't a valid parameter. Exiting"
				exit
				;;

			*)
				break
				;;
		esac
		shift 2
	done

	if [ $# -gt 1 ]; then
		index=0
		while [ $# -ne 1 ]; do
			COMMAND[index]="$1" # The intension is to get all arguments, but the last one.
			shift 1
			index=$(( $index + 1 ))
		done
		if [ -z ]
			SERVER="$1"

	else
		echo "Wrong syntax."
		help
	fi

	if [ -z $CLIENT ]; then
		CLIENT="$(lshw | grep "ip=" | cut -b 104-116)"
	fi

	connect ${COMMAND[@]} $SERVER
}

main $@
