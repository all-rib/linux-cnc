# Must install "dnsutils" first
ping_addr()
{
	PING_COUNT=$(ping -qc $PINGS "$SERVER" | grep "loss" | cut -b 24-24) #Will ping 4 packets to $1, and cut the value of answered packets
	if [ "$PING_COUNT" == "$PINGS" ]; then #If all packets returned, the $1 is online
		echo "0"
	else
		echo "1" #Or not
	fi
}

gen_key()	# Generate my key
{
	ssh-keygen -f "$KEY" -t rsa "$PASSWD"
}

send_key()	# Add my key
{
	ssh-copy-id -i "$KEY" -p $PORT "$SERVER"
}

ssh_send()	# Send text file
{
	scp -i "$KEY" -P $PORT "$GETFROM" "$SERVER:$SAVEIN"
}

ssh_get()	# Get text-file
{
	scp -i "$KEY" -P $PORT "$SERVER:$GETFROM" "$SAVEIN"
}

ssh_append()	# Append to text file
{
	ssh -p $PORT "$SERVER" "echo -e \"$MESSAGE\" >> $SAVEIN"
}

ssh_exec()	# Execute command
{
	ssh -p $PORT "$SERVER" "$COMMAND"
}

prepared()
{
	if [ -z $KEY ] || [ -z $SERVER ]; then
		exit
	fi

	if [ -z $GETFROM ]; then
		if [ -z $SAVEIN ]; then
			exit
		fi
		GETFROM="$SAVEIN"
	elif [ -z $SAVEIN ]; then
		SAVEIN="$GETFROM"
	fi

}
